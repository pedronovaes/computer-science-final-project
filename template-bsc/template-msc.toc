\select@language {brazil}
\contentsline {chapter}{Cap\'{\i }tulo~1---Introdu\IeC {\c c}\IeC {\~a}o}{1}
\contentsline {section}{\numberline {1.1}Introdu\IeC {\c c}\IeC {\~a}o}{1}
\contentsline {section}{\numberline {1.2}Motiva\IeC {\c c}\IeC {\~a}o}{3}
\contentsline {section}{\numberline {1.3}Objetivos}{3}
\contentsline {section}{\numberline {1.4}Organiza\IeC {\c c}\IeC {\~a}o do Trabalho}{4}
\contentsline {chapter}{Cap\'{\i }tulo~2---Fundamenta\IeC {\c c}\IeC {\~a}o te\IeC {\'o}rica}{5}
\contentsline {section}{\numberline {2.1}Pareamento de Registros}{5}
\contentsline {subsection}{\numberline {2.1.1}M\IeC {\'e}todo Probabil\IeC {\'\i }stico}{6}
\contentsline {subsection}{\numberline {2.1.2}Filtros de Bloom}{8}
\contentsline {subsection}{\numberline {2.1.3}C\IeC {\'a}lculo de Similaridade e Classifica\IeC {\c c}\IeC {\~a}o}{8}
\contentsline {section}{\numberline {2.2}Computa\IeC {\c c}\IeC {\~a}o de Alto Desempenho}{9}
\contentsline {subsection}{\numberline {2.2.1}Conceitos na Computa\IeC {\c c}\IeC {\~a}o Paralela}{10}
\contentsline {subsection}{\numberline {2.2.2}Computa\IeC {\c c}\IeC {\~a}o para Multicore}{12}
\contentsline {subsubsection}{\numberline {2.2.2.1}MPI}{12}
\contentsline {subsubsection}{\numberline {2.2.2.2}OpenMP}{12}
\contentsline {subsection}{\numberline {2.2.3}Computa\IeC {\c c}\IeC {\~a}o para GPU}{14}
\contentsline {subsubsection}{\numberline {2.2.3.1}CUDA}{15}
\contentsline {subsection}{\numberline {2.2.4}Computa\IeC {\c c}\IeC {\~a}o heterog\IeC {\^e}nea}{18}
